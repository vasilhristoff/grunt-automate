module.exports = function (grunt) {
    const PROJECT_DIR = 'project';
    const OUTPUT_DIR = 'output';

    /*  Tinypng */
    const API_KEY = '*';
    const OUTPUT_IMG_FOLDER = `./${OUTPUT_DIR}/tiny_png_jpg/`;

    /*  Purifycss */
    const FILES_USING_CSS = [`${PROJECT_DIR}/*.html`, `${PROJECT_DIR}/js/*.js`];
    const OUTPUT_CSS_FILE = `./${OUTPUT_DIR}/style_pure.css`;

    /*  Unused */
    const REPORT_FILE = 'output/report.txt';
    const DIRS_CONTAINING_REFERENCED_FILES = [`${PROJECT_DIR}/*.html`, `${PROJECT_DIR}/css/*.css`, `${PROJECT_DIR}/js/*.js`];

    var regexCss = /.\.css$/;    

    grunt.initConfig({
        prompt: {
            userinput: {
                options: {
                    questions: [{
                            config: 'imageDir',
                            type: 'input',
                            message: '---- (TinyPNG) Enter directory name of your image folder: ',
                            validate: function (value) {
                                if (value == '') {
                                    return 'You must enter a value to continue';
                                }

                                return true;
                            }
                        },
                        {
                            config: 'cssDir',
                            type: 'input',
                            message: '---- (PurifyCSS) Enter the name of your css directory: ',
                            validate: function (value) {
                                if (value == '') {
                                    return '!!! You must enter a value to continue.';
                                }

                                return true;
                            }
                        },
                        {
                            config: 'cssFile',
                            type: 'input',
                            message: '---- (PurifyCSS) Enter the name of the css file: ',
                            validate: function (value) {
                                var fileExtensionCheck = regexCss.test(value);
                                if (value == '') {
                                    return '!!! You must enter a value to continue.';
                                } else if(fileExtensionCheck == false) {
                                    return '!!! File extension must be .css';
                                }


                                return true;
                            }
                        },
                        {
                            config: 'removeFilesBool',
                            type: 'input',
                            message: '---- (Unused) Do you want to remove files which are repeating("true" or "false"): ',
                            validate: function (value) {
                                let removeVal = String(value);
                                if (removeVal == '') {
                                    return '!!! You must enter a value to continue.';
                                } else if(removeVal !== 'true' && removeVal !== 'false') {
                                    return '!!! The value should be boolean(true or false)';
                                }

                                return true;
                            }
                        }
                    ]
                }
            }
        },

        tinypng: {
            options: {
                apiKey: API_KEY,
                checkSigs: true,
                sigFile: 'dest/file_sigs.json',
                summarize: true,
                showProgress: true,
                stopOnImageError: false
            },
            compress: {
                expand: true,
                src: ['**/*.{png,jpg,jpeg}'],
                cwd: `./${PROJECT_DIR}/<%= imageDir %>/`,
                dest: OUTPUT_IMG_FOLDER
            }
        },

        purifycss: {
            options: {},
            target: {
                src: FILES_USING_CSS,
                css: [`${PROJECT_DIR}/<%= cssDir %>/<%= cssFile %>`],
                dest: OUTPUT_CSS_FILE
            }
        },

        unused: {
            options: {
                reference: `${PROJECT_DIR}/<%= imageDir %>/`,
                directory: DIRS_CONTAINING_REFERENCED_FILES,
                remove: Boolean('<%= removeFilesBool %>'),
                reportOutput: REPORT_FILE,
                fail: false
            }
        }
    });

    grunt.loadNpmTasks('grunt-tinypng');
    grunt.loadNpmTasks('grunt-purifycss');
    grunt.loadNpmTasks('grunt-unused');
    grunt.loadNpmTasks('grunt-prompt');

    grunt.registerTask("default", ["prompt", "purifycss", "unused", "tinypng"]);
}